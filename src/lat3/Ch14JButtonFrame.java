/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lat3;

import lat2.*;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;

/**
 *
 * @author lenovo
 */
public class Ch14JButtonFrame extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 50;
    private static final int BUTTON_HEIGHT = 30;
    private JButton button;
    private JButton button2;

    public static void main(String[] args) {
        Ch14JButtonFrame frame = new Ch14JButtonFrame();
        frame.setVisible(true);
    }

    public Ch14JButtonFrame() {

        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Program Ch7ButtonFrame");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        button = new JButton("Click Me");
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);

        button2 = new JButton("Tombol2");
        button2.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button2);

        ButtonHandler handler = new ButtonHandler();
        button.addActionListener(handler);
        button2.addActionListener(handler);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();

                  JButton buttonText = (JButton) e.getSource();
            
            JRootPane rootPane=clickedButton.getRootPane();
            Frame frame = (JFrame) rootPane.getParent();
        
        if (buttonText.equals("Click Me")) {
            frame.setTitle("(Dibuat dengan cara - 2) You Clicked " + buttonText);
        } else {
            frame.setTitle("You clicked " + buttonText);
        }
        button.addActionListener(this);
        button2.addActionListener(this);

    }

}
