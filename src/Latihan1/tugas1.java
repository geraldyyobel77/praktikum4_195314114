/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JTextField;

/**
 *
 * @author lenovo
 */
public class tugas1 extends JFrame implements ActionListener {

    private JLabel bil1;
    private JLabel bil2;
    private JLabel hasil;
    private JTextField text_n1;
    private JTextField text_n2;
    private JTextField text_hsl;
    private JButton button_jumlah;

    public static void main(String[] args) {
        tugas1 frame = new tugas1();
        frame.setVisible(true);
    }

    public tugas1() {
        Container contenPane = getContentPane();
        contenPane.setLayout(null);
        setSize(350, 210);
        setResizable(false);
        setTitle("input data");
        setLocation(150, 250);

        bil1 = new JLabel("Bilangan 1");
        bil1.setBounds(20, 15, 80, 30);
        contenPane.add(bil1);

        bil2 = new JLabel("Bilangan 2");
        bil2.setBounds(20, 50, 80, 30);
        contenPane.add(bil2);

        hasil = new JLabel("hasil");
        hasil.setBounds(20, 85, 80, 30);
        contenPane.add(hasil);

        text_n1 = new JTextField();
        text_n1.setBounds(140, 15, 80, 20);
        contenPane.add(text_n1);

        text_n2 = new JTextField();
        text_n2.setBounds(140, 50, 80, 20);
        contenPane.add(text_n2);

        text_hsl = new JTextField();
        text_hsl.setBounds(140, 85, 80, 20);
        contenPane.add(text_hsl);

        button_jumlah = new JButton("Jumlah");
        button_jumlah.setBounds(140, 125, 80, 30);
        button_jumlah.addActionListener(this);
        contenPane.add(button_jumlah);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();

        JRootPane rootPane = clickedButton.getRootPane();
        Frame frame = (JFrame) rootPane.getParent();
        String buttonText = clickedButton.getText();

        double n1 = Double.parseDouble(text_n1.getText());
        double n2 = Double.parseDouble(text_n2.getText());

        double jumlah = n1 + n2;

        text_hsl.setText(" " + jumlah);
    }

}
